package main.java.com.tsc.apogasiy.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProjectById();

    void unbindTaskByProjectId();

    void showTaskByProjectId();

    void removeProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

}
