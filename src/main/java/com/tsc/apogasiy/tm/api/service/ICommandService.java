package main.java.com.tsc.apogasiy.tm.api.service;

import main.java.com.tsc.apogasiy.tm.model.Command;

public interface ICommandService {

    Command[] getCommands();

}
